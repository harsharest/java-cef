#!/bin/bash
# Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
# reserved. Use of this source code is governed by a BSD-style license
# that can be found in the LICENSE file.

if [ -z "$1" ]; then
  echo "ERROR: Please specify a binaries path"
else

strip -s $1/libcef.so
strip -s $1/libEGL.so
strip -s $1/libGLESv2.so
strip -s $1/libjcef.so
fi
